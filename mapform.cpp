#include "mapform.h"


MapForm::MapForm(QWidget *parent) :
    QWidget(parent) {
    setupUi(this);

    splineSeries = new QSplineSeries;
    splineSeries2 = new QSplineSeries;
    scatterSeries = new QScatterSeries;

    xAxis = new QValueAxis;
    yAxis = new QValueAxis;
    chart = new QChart;
    chart->addSeries(splineSeries);
    chart->addSeries(splineSeries2);
    chart->addSeries(scatterSeries);

    chart->setAxisX(xAxis,splineSeries);
    chart->setAxisY(yAxis,splineSeries);
    chart->setAxisX(xAxis,splineSeries2);
    chart->setAxisY(yAxis,splineSeries2);
    chart->setAxisX(xAxis,scatterSeries);
    chart->setAxisY(yAxis,scatterSeries);

    chartView = new QChartView(this);

    hlay = new QHBoxLayout();
    hlay->addWidget(chartView);
    setLayout(hlay);
    chartView->setRenderHint(QPainter::Antialiasing);
    chartView->setChart(chart);

    xAxis->setRange(-100,100);
    yAxis->setRange(-100,100);

    xAxis->setTickCount(11);
    yAxis->setTickCount(11);

    xAxis->setTitleText("X, м");
    xAxis->setTitleText("Y, м");

//    splineSeries->append(0,0);

    scatterSeries->append(0,0);
    scatterSeries->markerShape();

}

MapForm::~MapForm() {

}

void MapForm::setXY(float X, float Y) {

    splineSeries->append(X,Y);
//    splineSeries2->append(X,Y+20);
    scatterSeries->clear();
    scatterSeries->append(X,Y);
    this->X = X;
    this->Y = Y;

}

void MapForm::moveRight(){
    chart->scroll(10,0);
}

void MapForm::moveLeft(){
    chart->scroll(-10,0);

}
void MapForm::scalePlus(){
    chart->zoomIn();
}

void MapForm::scaleMinus(){
    chartView->chart()->zoomOut();

}
void MapForm::moveUp(){
    chart->scroll(0,10);

}

void MapForm::moveDown(){
    chartView->chart()->scroll(0,-10);
}
void MapForm::clearChart(){
    scaleReset();
    scatterSeries->clear();
    splineSeries->clear();
    splineSeries2->clear();
    splineSeries->append(X,Y);
    scatterSeries->append(X,Y);
    splineSeries2->append(X,Y);


}
void MapForm::scaleReset(){
    chart->zoomReset();
}
