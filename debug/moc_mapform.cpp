/****************************************************************************
** Meta object code from reading C++ file 'mapform.h'
**
** Created by: The Qt Meta Object Compiler version 67 (Qt 5.8.0)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../mapform.h"
#include <QtCore/qbytearray.h>
#include <QtCore/qmetatype.h>
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'mapform.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 67
#error "This file was generated using the moc from 5.8.0. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
QT_WARNING_PUSH
QT_WARNING_DISABLE_DEPRECATED
struct qt_meta_stringdata_MapForm_t {
    QByteArrayData data[13];
    char stringdata0[97];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_MapForm_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_MapForm_t qt_meta_stringdata_MapForm = {
    {
QT_MOC_LITERAL(0, 0, 7), // "MapForm"
QT_MOC_LITERAL(1, 8, 5), // "setXY"
QT_MOC_LITERAL(2, 14, 0), // ""
QT_MOC_LITERAL(3, 15, 1), // "X"
QT_MOC_LITERAL(4, 17, 1), // "Y"
QT_MOC_LITERAL(5, 19, 8), // "moveDown"
QT_MOC_LITERAL(6, 28, 6), // "moveUp"
QT_MOC_LITERAL(7, 35, 10), // "scaleMinus"
QT_MOC_LITERAL(8, 46, 9), // "scalePlus"
QT_MOC_LITERAL(9, 56, 8), // "moveLeft"
QT_MOC_LITERAL(10, 65, 9), // "moveRight"
QT_MOC_LITERAL(11, 75, 10), // "clearChart"
QT_MOC_LITERAL(12, 86, 10) // "scaleReset"

    },
    "MapForm\0setXY\0\0X\0Y\0moveDown\0moveUp\0"
    "scaleMinus\0scalePlus\0moveLeft\0moveRight\0"
    "clearChart\0scaleReset"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_MapForm[] = {

 // content:
       7,       // revision
       0,       // classname
       0,    0, // classinfo
       9,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

 // slots: name, argc, parameters, tag, flags
       1,    2,   59,    2, 0x0a /* Public */,
       5,    0,   64,    2, 0x0a /* Public */,
       6,    0,   65,    2, 0x0a /* Public */,
       7,    0,   66,    2, 0x0a /* Public */,
       8,    0,   67,    2, 0x0a /* Public */,
       9,    0,   68,    2, 0x0a /* Public */,
      10,    0,   69,    2, 0x0a /* Public */,
      11,    0,   70,    2, 0x0a /* Public */,
      12,    0,   71,    2, 0x0a /* Public */,

 // slots: parameters
    QMetaType::Void, QMetaType::Float, QMetaType::Float,    3,    4,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,

       0        // eod
};

void MapForm::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        MapForm *_t = static_cast<MapForm *>(_o);
        Q_UNUSED(_t)
        switch (_id) {
        case 0: _t->setXY((*reinterpret_cast< float(*)>(_a[1])),(*reinterpret_cast< float(*)>(_a[2]))); break;
        case 1: _t->moveDown(); break;
        case 2: _t->moveUp(); break;
        case 3: _t->scaleMinus(); break;
        case 4: _t->scalePlus(); break;
        case 5: _t->moveLeft(); break;
        case 6: _t->moveRight(); break;
        case 7: _t->clearChart(); break;
        case 8: _t->scaleReset(); break;
        default: ;
        }
    }
}

const QMetaObject MapForm::staticMetaObject = {
    { &QWidget::staticMetaObject, qt_meta_stringdata_MapForm.data,
      qt_meta_data_MapForm,  qt_static_metacall, Q_NULLPTR, Q_NULLPTR}
};


const QMetaObject *MapForm::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *MapForm::qt_metacast(const char *_clname)
{
    if (!_clname) return Q_NULLPTR;
    if (!strcmp(_clname, qt_meta_stringdata_MapForm.stringdata0))
        return static_cast<void*>(const_cast< MapForm*>(this));
    if (!strcmp(_clname, "Ui::MapForm"))
        return static_cast< Ui::MapForm*>(const_cast< MapForm*>(this));
    return QWidget::qt_metacast(_clname);
}

int MapForm::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QWidget::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 9)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 9;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 9)
            *reinterpret_cast<int*>(_a[0]) = -1;
        _id -= 9;
    }
    return _id;
}
QT_WARNING_POP
QT_END_MOC_NAMESPACE
