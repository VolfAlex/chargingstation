#-------------------------------------------------
#
# Project created by QtCreator 2018-12-21T15:31:03
#
#-------------------------------------------------

QT       += core gui charts

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = SrrrrrrrrrrrMap
TEMPLATE = app


SOURCES += main.cpp\
        widget.cpp \
    mapform.cpp \
    form.cpp

HEADERS  += widget.h \
    mapform.h \
    form.h

FORMS    += widget.ui \
    mapform.ui \
    form.ui
