#ifndef WIDGET_H
#define WIDGET_H

#include <QWidget>
#include "ui_widget.h"
#include "form.h"


class Widget : public QWidget, public Ui::Widget {
    Q_OBJECT

public:
    explicit Widget(QWidget *parent = 0);
    ~Widget();

public slots:
    void startTimer(bool);
    void tick();
signals:
    void setXY(float X, float Y);
private:
    Ui::Widget *ui;
    Form *myform;
    int dt;
    QTimer *time;
    float X1,Y1,X,Y,V,psi,fi,SX,SY;

};

#endif // WIDGET_H
