#ifndef MAPFORM_H
#define MAPFORM_H

#include <QWidget>
#include "ui_mapform.h"
#include <QtCharts>
#include <QHBoxLayout>

/*namespace Ui {
class Widget;
}

class Widget : public QWidget
{
    Q_OBJECT

public:
    explicit Widget(QWidget *parent = 0);
    ~Widget();

    public slots:
        void startTimer(bool);
        void tick();

private:
    Ui::Widget *ui;
    Form *myform;
        int dt;
        QTimer *time;
        float X,Y,V,psi;


signals:
    void sendData(QString str);
    void setXY(float X, float Y);
private slots:
    void onButtonSend();
    void on_pushButton_2_clicked();
    void on_wgtMap_destroyed();
};

//#endif // WIDGET_H*/
using namespace QtCharts;
class MapForm : public QWidget, public Ui::MapForm {
    Q_OBJECT


private:

    QChartView * chartView;

public:
    explicit MapForm(QWidget *parent = 0);
    ~MapForm();


public slots:
    void setXY(float X,float Y);
    void moveDown();
    void moveUp();
    void scaleMinus();
    void scalePlus();
    void moveLeft();
    void moveRight();
    void clearChart();
    void scaleReset();
private:
    QHBoxLayout* hlay;
    QChart* chart;
    QSplineSeries* splineSeries;
    QSplineSeries* splineSeries2;
    QValueAxis* xAxis;
    QValueAxis* yAxis;
    QScatterSeries* scatterSeries;
    float X, Y;


};

#endif // MAPFORM_H
