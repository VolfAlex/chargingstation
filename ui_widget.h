/********************************************************************************
** Form generated from reading UI file 'widget.ui'
**
** Created by: Qt User Interface Compiler version 5.8.0
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_WIDGET_H
#define UI_WIDGET_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QFormLayout>
#include <QtWidgets/QHBoxLayout>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLabel>
#include <QtWidgets/QLineEdit>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QToolButton>
#include <QtWidgets/QVBoxLayout>
#include <QtWidgets/QWidget>
#include "mapform.h"

QT_BEGIN_NAMESPACE

class Ui_Widget
{
public:
    QHBoxLayout *horizontalLayout_2;
    MapForm *wgtMap;
    QWidget *widget;
    QHBoxLayout *horizontalLayout;
    QVBoxLayout *verticalLayout_3;
    QVBoxLayout *verticalLayout;
    QToolButton *btnScaleMinus;
    QToolButton *btnScalePlus;
    QToolButton *btnLeft;
    QToolButton *btnRight;
    QToolButton *btnUp;
    QToolButton *btnDown;
    QToolButton *btnClear;
    QFormLayout *formLayout;
    QLabel *label_3;
    QLineEdit *edtX;
    QLabel *label_4;
    QLineEdit *edtY;
    QLineEdit *edtV;
    QLabel *label;
    QLabel *label_2;
    QLineEdit *edtPsi;
    QVBoxLayout *verticalLayout_2;
    QPushButton *btnGotoCamera;
    QPushButton *btnStartTimer;

    void setupUi(QWidget *Widget)
    {
        if (Widget->objectName().isEmpty())
            Widget->setObjectName(QStringLiteral("Widget"));
        Widget->resize(655, 448);
        QSizePolicy sizePolicy(QSizePolicy::Preferred, QSizePolicy::Expanding);
        sizePolicy.setHorizontalStretch(0);
        sizePolicy.setVerticalStretch(0);
        sizePolicy.setHeightForWidth(Widget->sizePolicy().hasHeightForWidth());
        Widget->setSizePolicy(sizePolicy);
        QFont font;
        font.setPointSize(10);
        Widget->setFont(font);
        horizontalLayout_2 = new QHBoxLayout(Widget);
        horizontalLayout_2->setSpacing(6);
        horizontalLayout_2->setContentsMargins(11, 11, 11, 11);
        horizontalLayout_2->setObjectName(QStringLiteral("horizontalLayout_2"));
        wgtMap = new MapForm(Widget);
        wgtMap->setObjectName(QStringLiteral("wgtMap"));
        QSizePolicy sizePolicy1(QSizePolicy::Expanding, QSizePolicy::Preferred);
        sizePolicy1.setHorizontalStretch(0);
        sizePolicy1.setVerticalStretch(0);
        sizePolicy1.setHeightForWidth(wgtMap->sizePolicy().hasHeightForWidth());
        wgtMap->setSizePolicy(sizePolicy1);

        horizontalLayout_2->addWidget(wgtMap);

        widget = new QWidget(Widget);
        widget->setObjectName(QStringLiteral("widget"));
        widget->setMaximumSize(QSize(150, 16777215));
        horizontalLayout = new QHBoxLayout(widget);
        horizontalLayout->setSpacing(6);
        horizontalLayout->setContentsMargins(11, 11, 11, 11);
        horizontalLayout->setObjectName(QStringLiteral("horizontalLayout"));
        verticalLayout_3 = new QVBoxLayout();
        verticalLayout_3->setSpacing(6);
        verticalLayout_3->setObjectName(QStringLiteral("verticalLayout_3"));
        verticalLayout = new QVBoxLayout();
        verticalLayout->setSpacing(6);
        verticalLayout->setObjectName(QStringLiteral("verticalLayout"));
        btnScaleMinus = new QToolButton(widget);
        btnScaleMinus->setObjectName(QStringLiteral("btnScaleMinus"));
        sizePolicy.setHeightForWidth(btnScaleMinus->sizePolicy().hasHeightForWidth());
        btnScaleMinus->setSizePolicy(sizePolicy);
        btnScaleMinus->setMinimumSize(QSize(100, 0));
        btnScaleMinus->setIconSize(QSize(30, 30));

        verticalLayout->addWidget(btnScaleMinus);

        btnScalePlus = new QToolButton(widget);
        btnScalePlus->setObjectName(QStringLiteral("btnScalePlus"));
        sizePolicy.setHeightForWidth(btnScalePlus->sizePolicy().hasHeightForWidth());
        btnScalePlus->setSizePolicy(sizePolicy);
        btnScalePlus->setMinimumSize(QSize(100, 0));
        btnScalePlus->setIconSize(QSize(30, 30));
        btnScalePlus->setAutoExclusive(false);

        verticalLayout->addWidget(btnScalePlus);

        btnLeft = new QToolButton(widget);
        btnLeft->setObjectName(QStringLiteral("btnLeft"));
        sizePolicy.setHeightForWidth(btnLeft->sizePolicy().hasHeightForWidth());
        btnLeft->setSizePolicy(sizePolicy);
        btnLeft->setMinimumSize(QSize(100, 0));

        verticalLayout->addWidget(btnLeft);

        btnRight = new QToolButton(widget);
        btnRight->setObjectName(QStringLiteral("btnRight"));
        sizePolicy.setHeightForWidth(btnRight->sizePolicy().hasHeightForWidth());
        btnRight->setSizePolicy(sizePolicy);
        btnRight->setMinimumSize(QSize(100, 0));

        verticalLayout->addWidget(btnRight);

        btnUp = new QToolButton(widget);
        btnUp->setObjectName(QStringLiteral("btnUp"));
        sizePolicy.setHeightForWidth(btnUp->sizePolicy().hasHeightForWidth());
        btnUp->setSizePolicy(sizePolicy);
        btnUp->setMinimumSize(QSize(100, 0));

        verticalLayout->addWidget(btnUp);

        btnDown = new QToolButton(widget);
        btnDown->setObjectName(QStringLiteral("btnDown"));
        sizePolicy.setHeightForWidth(btnDown->sizePolicy().hasHeightForWidth());
        btnDown->setSizePolicy(sizePolicy);
        btnDown->setMinimumSize(QSize(100, 0));

        verticalLayout->addWidget(btnDown);

        btnClear = new QToolButton(widget);
        btnClear->setObjectName(QStringLiteral("btnClear"));
        sizePolicy.setHeightForWidth(btnClear->sizePolicy().hasHeightForWidth());
        btnClear->setSizePolicy(sizePolicy);
        btnClear->setMinimumSize(QSize(100, 0));

        verticalLayout->addWidget(btnClear);


        verticalLayout_3->addLayout(verticalLayout);

        formLayout = new QFormLayout();
        formLayout->setSpacing(6);
        formLayout->setObjectName(QStringLiteral("formLayout"));
        formLayout->setFieldGrowthPolicy(QFormLayout::AllNonFixedFieldsGrow);
        label_3 = new QLabel(widget);
        label_3->setObjectName(QStringLiteral("label_3"));

        formLayout->setWidget(2, QFormLayout::LabelRole, label_3);

        edtX = new QLineEdit(widget);
        edtX->setObjectName(QStringLiteral("edtX"));
        QSizePolicy sizePolicy2(QSizePolicy::Fixed, QSizePolicy::Fixed);
        sizePolicy2.setHorizontalStretch(0);
        sizePolicy2.setVerticalStretch(0);
        sizePolicy2.setHeightForWidth(edtX->sizePolicy().hasHeightForWidth());
        edtX->setSizePolicy(sizePolicy2);
        edtX->setMaximumSize(QSize(100, 16777215));

        formLayout->setWidget(2, QFormLayout::FieldRole, edtX);

        label_4 = new QLabel(widget);
        label_4->setObjectName(QStringLiteral("label_4"));

        formLayout->setWidget(3, QFormLayout::LabelRole, label_4);

        edtY = new QLineEdit(widget);
        edtY->setObjectName(QStringLiteral("edtY"));
        sizePolicy2.setHeightForWidth(edtY->sizePolicy().hasHeightForWidth());
        edtY->setSizePolicy(sizePolicy2);
        edtY->setMaximumSize(QSize(100, 16777215));

        formLayout->setWidget(3, QFormLayout::FieldRole, edtY);

        edtV = new QLineEdit(widget);
        edtV->setObjectName(QStringLiteral("edtV"));
        sizePolicy2.setHeightForWidth(edtV->sizePolicy().hasHeightForWidth());
        edtV->setSizePolicy(sizePolicy2);
        edtV->setMaximumSize(QSize(100, 16777215));

        formLayout->setWidget(4, QFormLayout::FieldRole, edtV);

        label = new QLabel(widget);
        label->setObjectName(QStringLiteral("label"));
        QSizePolicy sizePolicy3(QSizePolicy::Preferred, QSizePolicy::Fixed);
        sizePolicy3.setHorizontalStretch(0);
        sizePolicy3.setVerticalStretch(0);
        sizePolicy3.setHeightForWidth(label->sizePolicy().hasHeightForWidth());
        label->setSizePolicy(sizePolicy3);

        formLayout->setWidget(4, QFormLayout::LabelRole, label);

        label_2 = new QLabel(widget);
        label_2->setObjectName(QStringLiteral("label_2"));
        sizePolicy2.setHeightForWidth(label_2->sizePolicy().hasHeightForWidth());
        label_2->setSizePolicy(sizePolicy2);

        formLayout->setWidget(6, QFormLayout::LabelRole, label_2);

        edtPsi = new QLineEdit(widget);
        edtPsi->setObjectName(QStringLiteral("edtPsi"));
        sizePolicy2.setHeightForWidth(edtPsi->sizePolicy().hasHeightForWidth());
        edtPsi->setSizePolicy(sizePolicy2);
        edtPsi->setMaximumSize(QSize(100, 16777215));

        formLayout->setWidget(6, QFormLayout::FieldRole, edtPsi);


        verticalLayout_3->addLayout(formLayout);

        verticalLayout_2 = new QVBoxLayout();
        verticalLayout_2->setSpacing(6);
        verticalLayout_2->setObjectName(QStringLiteral("verticalLayout_2"));
        btnGotoCamera = new QPushButton(widget);
        btnGotoCamera->setObjectName(QStringLiteral("btnGotoCamera"));

        verticalLayout_2->addWidget(btnGotoCamera);

        btnStartTimer = new QPushButton(widget);
        btnStartTimer->setObjectName(QStringLiteral("btnStartTimer"));
        sizePolicy3.setHeightForWidth(btnStartTimer->sizePolicy().hasHeightForWidth());
        btnStartTimer->setSizePolicy(sizePolicy3);
        btnStartTimer->setCheckable(true);

        verticalLayout_2->addWidget(btnStartTimer);


        verticalLayout_3->addLayout(verticalLayout_2);


        horizontalLayout->addLayout(verticalLayout_3);


        horizontalLayout_2->addWidget(widget);


        retranslateUi(Widget);

        QMetaObject::connectSlotsByName(Widget);
    } // setupUi

    void retranslateUi(QWidget *Widget)
    {
        Widget->setWindowTitle(QApplication::translate("Widget", "\320\232\320\260\321\200\321\202\320\260", Q_NULLPTR));
        btnScaleMinus->setText(QApplication::translate("Widget", "-", Q_NULLPTR));
        btnScalePlus->setText(QApplication::translate("Widget", "+", Q_NULLPTR));
        btnLeft->setText(QApplication::translate("Widget", "\342\206\220", Q_NULLPTR));
        btnRight->setText(QApplication::translate("Widget", "\342\206\222", Q_NULLPTR));
        btnUp->setText(QApplication::translate("Widget", "\342\206\221", Q_NULLPTR));
        btnDown->setText(QApplication::translate("Widget", "\342\206\223", Q_NULLPTR));
        btnClear->setText(QApplication::translate("Widget", "\320\236\321\207\320\270\321\201\321\202\320\270\321\202\321\214", Q_NULLPTR));
        label_3->setText(QApplication::translate("Widget", "X = ", Q_NULLPTR));
        label_4->setText(QApplication::translate("Widget", "Y= ", Q_NULLPTR));
        label->setText(QApplication::translate("Widget", "V=", Q_NULLPTR));
        label_2->setText(QApplication::translate("Widget", "\320\232\321\203\321\200\321\201=", Q_NULLPTR));
        btnGotoCamera->setText(QApplication::translate("Widget", "\320\232\320\260\320\274\320\265\321\200\320\260", Q_NULLPTR));
        btnStartTimer->setText(QApplication::translate("Widget", "\320\241\321\202\320\260\321\200\321\202", Q_NULLPTR));
    } // retranslateUi

};

namespace Ui {
    class Widget: public Ui_Widget {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_WIDGET_H
