#ifndef FORM_H
#define FORM_H

#include <QWidget>

namespace Ui {
class Form;
}

class Form : public QWidget
{
    Q_OBJECT

public:
    explicit Form(QWidget *parent = 0);
    ~Form();

private:
    Ui::Form *ui;

private slots:
    //void onButtonSend();
    //void on_pushButton_2_clicked();
    void on_Accept_clicked();
};

#endif // FORM_H
