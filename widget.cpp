#include "widget.h"
#include "form.h"
#include "ui_widget.h"
#include <QMessageBox>
#include <QPixmap>
#include <math.h>


Widget::Widget(QWidget *parent) :
    QWidget(parent)
    ,ui(new Ui::Widget)

{
    //****************************************************************************************************************

   setupUi(this);
   myform = new Form(); // создаем нашу форму

   connect(btnGotoCamera, SIGNAL(clicked()), myform, SLOT(show())); // подключаем сигнал к слоту

//   connect(btnGotoCamera, SIGNAL(clicked()), this, SLOT(onButtonSend())); // подключаем клик по кнопке к определенному нами слоту
//   connect(this, SIGNAL(sendData(QString)), myform, SLOT(recieveData(QString))); // подключение сигнала к слоту нашей формы
    //*****************************************************************************************************************

    dt=5;
    V=0; psi=0; X=0; Y=0; X1=50; Y1=-50;

    time = new QTimer ();
    //setupUi(this);
    connect(btnStartTimer,SIGNAL(clicked(bool)),SLOT(startTimer(bool)));
    connect(time,SIGNAL(timeout()), SLOT(tick()));
    connect(this, SIGNAL(setXY(float,float)),wgtMap,SLOT(setXY(float,float)));
    connect(btnDown,&QPushButton::clicked,wgtMap,&MapForm::moveDown);
    connect(btnUp,&QPushButton::clicked,wgtMap,&MapForm::moveUp);
    connect(btnRight,&QPushButton::clicked,wgtMap,&MapForm::moveRight);
    connect(btnLeft,&QPushButton::clicked,wgtMap,&MapForm::moveLeft);
    connect(btnLeft,&QPushButton::clicked,wgtMap,&MapForm::moveLeft);
    connect(btnScalePlus,&QPushButton::clicked,wgtMap,&MapForm::scalePlus);
    connect(btnScaleMinus,&QPushButton::clicked,wgtMap,&MapForm::scaleMinus);
    connect(btnClear,&QPushButton::clicked,wgtMap,&MapForm::clearChart);



}

Widget::~Widget()
{

}

void Widget::startTimer(bool status)
{
    X = edtX->text().toFloat();
    Y = edtY->text().toFloat();
    if (status){
        V = edtV->text().toFloat();
        psi = edtPsi->text().toFloat();
        time->start(dt);
    }
    else time->stop();
}

void Widget::tick()
{

    SX=X1-X;
    SY=Y1-Y;

    if (SY>=0)
         fi=atan(SX/SY);
    else
        if (SX<0)
            fi=-M_PI+atan(SX/SY);
    else
            fi=M_PI+atan(SX/SY);



    X+=dt*0.001*V*sin(fi);
    Y+=dt*0.001*V*cos(fi);
    emit setXY(X,Y);
    qDebug()<<X<<" "<<Y;
}


