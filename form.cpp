#include "form.h"
#include "ui_form.h"
#include <QDebug>

Form::Form(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::Form)
{
    ui->setupUi(this);

    //Загружаем исходную картинку
    QPixmap pix ("C:/QtProjects/ChargingStation/66n1.jpg");
    update();

    int pixWidh = ui -> Image -> width();
    int pixHeight = ui -> Image -> height();

    qDebug()<<  pixWidh<< pixHeight;
    ui->Image -> setPixmap(pix.scaled(pixWidh, pixHeight, Qt::KeepAspectRatio));

    pixWidh = ui -> Image -> width();
    pixHeight = ui -> Image -> height();

    qDebug()<<  pixWidh<< pixHeight;
    ui->Image -> setPixmap(pix.scaled(pixWidh, pixHeight, Qt::KeepAspectRatio));


}

Form::~Form()
{
    delete ui;
}

void Form::on_Accept_clicked()
{
    int aperture_size = ui->comboBox_sobel->currentText().toInt() ; //размер оператора Собеля
    int min_line = ui->comboBox_min->currentText().toInt(); // минимальная длина линии
    int max_gap = ui->comboBox_max->currentText().toInt(); // максимальный промежуток между линиями, чтобы считалось за одну линию

    QPixmap pixHough ("C:/QtProjects/ChargingStation/images/3_50_5.png");

    switch (aperture_size)
    {
        case 3:
            switch (min_line)
            {
                case 50:
                    switch (max_gap)
                    {
                    case 5: pixHough.load("C:/QtProjects/ChargingStation/images/3_50_5.png");
                        break;
                    case 10: pixHough.load ("C:/QtProjects/ChargingStation/images/3_50_10.png");
                        break;
                    case 15: pixHough.load ("C:/QtProjects/ChargingStation/images/3_50_15.png");
                        break;
                    }
                    break;
                case 80:
                    switch (max_gap)
                    {
                    case 5: pixHough.load ("C:/QtProjects/ChargingStation/images/3_80_5.png");
                        break;
                    case 10: pixHough.load ("C:/QtProjects/ChargingStation/images/3_80_10.png");
                        break;
                    case 15: pixHough.load ("C:/QtProjects/ChargingStation/images/3_80_15.png");
                        break;
                    }
                    break;
                case 100:
                    switch (max_gap)
                    {
                    case 5: pixHough.load ("C:/QtProjects/ChargingStation/images/3_100_5.png");
                        break;
                    case 10: pixHough.load ("C:/QtProjects/ChargingStation/images/3_100_10.png");
                        break;
                    case 15: pixHough.load ("C:/QtProjects/ChargingStation/images/3_100_15.png");
                        break;
                    }
                    break;
            }
            break;
        case 5: pixHough.load ("C:/QtProjects/ChargingStation/images/5_80_10.png");
        break;
        case 7: pixHough.load ("C:/QtProjects/ChargingStation/images/7_80_10.png");
        break;
    }

    int HoughWidh = ui -> Image -> width();
    int HoughHeight = ui -> Image -> height();
    ui->Image -> setPixmap(pixHough.scaled(HoughWidh, HoughHeight, Qt::KeepAspectRatio));
    //QMessageBox::information(this, "Title", ui->comboBox_3->currentText());
}
