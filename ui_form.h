/********************************************************************************
** Form generated from reading UI file 'form.ui'
**
** Created by: Qt User Interface Compiler version 5.8.0
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_FORM_H
#define UI_FORM_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QComboBox>
#include <QtWidgets/QFormLayout>
#include <QtWidgets/QHBoxLayout>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLabel>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_Form
{
public:
    QWidget *widget;
    QHBoxLayout *horizontalLayout;
    QFormLayout *formLayout_2;
    QLabel *label_sobel;
    QComboBox *comboBox_sobel;
    QLabel *label_min;
    QComboBox *comboBox_min;
    QLabel *label_max;
    QComboBox *comboBox_max;
    QPushButton *Accept;
    QLabel *Image;

    void setupUi(QWidget *Form)
    {
        if (Form->objectName().isEmpty())
            Form->setObjectName(QStringLiteral("Form"));
        Form->resize(813, 361);
        QSizePolicy sizePolicy(QSizePolicy::Maximum, QSizePolicy::Maximum);
        sizePolicy.setHorizontalStretch(0);
        sizePolicy.setVerticalStretch(0);
        sizePolicy.setHeightForWidth(Form->sizePolicy().hasHeightForWidth());
        Form->setSizePolicy(sizePolicy);
        widget = new QWidget(Form);
        widget->setObjectName(QStringLiteral("widget"));
        widget->setGeometry(QRect(9, 9, 240, 121));
        widget->setMaximumSize(QSize(240, 16777215));
        horizontalLayout = new QHBoxLayout(widget);
        horizontalLayout->setObjectName(QStringLiteral("horizontalLayout"));
        formLayout_2 = new QFormLayout();
        formLayout_2->setObjectName(QStringLiteral("formLayout_2"));
        formLayout_2->setFieldGrowthPolicy(QFormLayout::AllNonFixedFieldsGrow);
        label_sobel = new QLabel(widget);
        label_sobel->setObjectName(QStringLiteral("label_sobel"));

        formLayout_2->setWidget(0, QFormLayout::LabelRole, label_sobel);

        comboBox_sobel = new QComboBox(widget);
        comboBox_sobel->setObjectName(QStringLiteral("comboBox_sobel"));

        formLayout_2->setWidget(0, QFormLayout::FieldRole, comboBox_sobel);

        label_min = new QLabel(widget);
        label_min->setObjectName(QStringLiteral("label_min"));

        formLayout_2->setWidget(1, QFormLayout::LabelRole, label_min);

        comboBox_min = new QComboBox(widget);
        comboBox_min->setObjectName(QStringLiteral("comboBox_min"));

        formLayout_2->setWidget(1, QFormLayout::FieldRole, comboBox_min);

        label_max = new QLabel(widget);
        label_max->setObjectName(QStringLiteral("label_max"));

        formLayout_2->setWidget(2, QFormLayout::LabelRole, label_max);

        comboBox_max = new QComboBox(widget);
        comboBox_max->setObjectName(QStringLiteral("comboBox_max"));

        formLayout_2->setWidget(2, QFormLayout::FieldRole, comboBox_max);

        Accept = new QPushButton(widget);
        Accept->setObjectName(QStringLiteral("Accept"));

        formLayout_2->setWidget(3, QFormLayout::FieldRole, Accept);


        horizontalLayout->addLayout(formLayout_2);

        Image = new QLabel(Form);
        Image->setObjectName(QStringLiteral("Image"));
        Image->setGeometry(QRect(255, 9, 549, 341));
        Image->setFrameShape(QFrame::NoFrame);
        Image->setLineWidth(0);

        retranslateUi(Form);

        QMetaObject::connectSlotsByName(Form);
    } // setupUi

    void retranslateUi(QWidget *Form)
    {
        Form->setWindowTitle(QApplication::translate("Form", "Form", Q_NULLPTR));
        label_sobel->setText(QApplication::translate("Form", "\320\240\320\260\320\267\320\274\320\265\321\200 \320\276\320\277\320\265\321\200\320\260\321\202\320\276\321\200\320\260 \320\241\320\276\320\261\320\265\320\273\321\217", Q_NULLPTR));
        comboBox_sobel->clear();
        comboBox_sobel->insertItems(0, QStringList()
         << QApplication::translate("Form", "3", Q_NULLPTR)
         << QApplication::translate("Form", "5", Q_NULLPTR)
         << QApplication::translate("Form", "7", Q_NULLPTR)
        );
        label_min->setText(QApplication::translate("Form", "\320\234\320\270\320\275\320\270\320\274\320\260\320\273\321\214\320\275\320\260\321\217 \320\264\320\273\320\270\320\275\320\260 \320\273\320\270\320\275\320\270\320\270", Q_NULLPTR));
        comboBox_min->clear();
        comboBox_min->insertItems(0, QStringList()
         << QApplication::translate("Form", "50", Q_NULLPTR)
         << QApplication::translate("Form", "80", Q_NULLPTR)
         << QApplication::translate("Form", "100", Q_NULLPTR)
        );
        label_max->setText(QApplication::translate("Form", "\320\234\320\260\320\272\321\201\320\270\320\274\320\260\320\273\321\214\320\275\321\213\320\271 \320\277\321\200\320\276\320\274\320\265\320\266\321\203\321\202\320\276\320\272", Q_NULLPTR));
        comboBox_max->clear();
        comboBox_max->insertItems(0, QStringList()
         << QApplication::translate("Form", "5", Q_NULLPTR)
         << QApplication::translate("Form", "10", Q_NULLPTR)
         << QApplication::translate("Form", "15", Q_NULLPTR)
        );
        Accept->setText(QApplication::translate("Form", "\320\237\321\200\320\270\320\275\321\217\321\202\321\214", Q_NULLPTR));
        Image->setText(QString());
    } // retranslateUi

};

namespace Ui {
    class Form: public Ui_Form {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_FORM_H
